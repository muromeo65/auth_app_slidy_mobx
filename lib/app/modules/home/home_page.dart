import 'package:auth_app_slidy_mobx/app/app_controller.dart';
import 'package:auth_app_slidy_mobx/app/modules/home/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final home_controller = HomeController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Observer(
            builder: (_) {
              return Text(
                '${home_controller.value}',
                style: TextStyle(fontSize: 34, color: Colors.black),
              );
            },
          )
        ],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          home_controller.increment();
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
